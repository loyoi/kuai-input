use plugin_interface::{Info, Plugin,PluginCtx};

#[no_mangle]
pub extern "Rust" fn new_plugin(name: String) -> Box<dyn Plugin> {
    Box::new(MyPlugin::new(name))
}

pub struct MyPlugin {
    id: String,
}

impl MyPlugin {
    fn new(name: String) -> MyPlugin {
        // let id = format!("{:08x}", rand::random::<u32>());
        println!("[{}] 创建实例!", name);
        MyPlugin {
            id: name,
        }
    }
}

impl Plugin for MyPlugin {
    fn run(&self, data: PluginCtx::Ctx) -> Result<String,  String> {
        println!("[{}] 实现的 run 方法! ; 收到的数据 {:?}", self.id, data);
        return Ok("my test".to_string());
    }
    
    fn get_info(&self) -> Info {
        Info::new(
            "插件测试 ".to_string(),
            "my_test".to_string(),
            "0.0.1".to_string(),
            "LoYoi".to_string(),
            "compile_date".to_string(),
            "description".to_string(),
            true,
        )
    }
}

impl Drop for MyPlugin {
    fn drop(&mut self) {
        println!("[{}] 销毁实例!", self.id);
    }
}
