use reqwest;
use serde_json::json;

struct Message {
    system: String,
    user: String,
}

impl Message {
    // 构造函数
    fn new(system: String, user: String) -> Self {
        Message { system, user }
    }
}

fn translate(text: String) -> Result<String, anyhow::Error> {
    let msg = "帮我翻译为中文，只需要给我翻译后的结果".to_string();
    let message = Message::new(msg.clone(), text);

    let client = reqwest::blocking::Client::new();
    let url = "https://api.gptgod.online/v1/chat/completions";

    let api_key = std::env::var("GPTGOD_KEY").unwrap_or_else(|_| {
        "".to_string() // 这里只是示例，实际中可能需要更合适的处理方式
    });

    let response = client
        .post(url)
        .bearer_auth(api_key)
        .json(&json!({
            "model": "gpt-3.5-turbo-16k",
            "messages": [
                {"role": "system", "content": message.system},
                {"role": "user", "content": message.user}
            ]
        }))
        .send()?;

    let response_text: serde_json::Value = response.json()?;

    Ok(response_text.to_string())
}

pub fn main(text: String) -> Result<String, String> {
    translate(text).map_err(|err| format!("{}", err))
}
