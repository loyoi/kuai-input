use plugin_interface::{Info, Plugin, PluginCtx};
mod my;

#[no_mangle]
pub extern "Rust" fn new_plugin(name: String) -> Box<dyn Plugin> {
    Box::new(MyPlugin::new(name))
}

pub struct MyPlugin {
    id: String,
}

impl MyPlugin {
    fn new(name: String) -> MyPlugin {
        println!("[{}] 创建实例!", name);
        MyPlugin { id: name }
    }
}

impl Plugin for MyPlugin {
    fn run(&self, data: PluginCtx::Ctx) -> Result<String, String> {
        println!("[{}] 开始运行! 选择的文本 ：{}", self.id, data.get_sel_text());
        my::main(data.get_sel_text())
    }

    fn get_info(&self) -> Info {
        Info::new(
            "ai翻译".to_string(),
            "ai_translate".to_string(),
            "0.0.1".to_string(),
            "".to_string(),
            "compile_date".to_string(),
            "description".to_string(),
            true,
        )
    }
}

impl Drop for MyPlugin {
    fn drop(&mut self) {
        println!("[{}] 销毁实例!", self.id);
    }
}
