#[derive(Debug, Clone)]
pub struct Ctx {
    /// 选择的文本
    sel_text: String,
}

impl Ctx {
    // fn new() -> Self {
    //     Self::default()
    // }

    /// 设置选择的文本
    pub fn set_sel_text(mut self, text: String) -> Self {
        self.sel_text = text;
        self
    }

    /// 获取选择的文本
    pub fn get_sel_text(&self) -> String {
        self.sel_text.clone()
    }
}

impl Default for Ctx {
    fn default() -> Self {
        Self {
            sel_text: "".to_string(),
        }
    }
}
