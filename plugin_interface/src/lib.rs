pub mod ctx;
pub use ctx as PluginCtx;

pub trait Plugin {
    /// 异步执行操作，并通过回调通知结果。
    fn run(&self, data: ctx::Ctx) -> Result<String, String>;
    fn get_info(&self) -> Info;
}

pub struct Api {}
impl Api {
    pub fn new() -> Api {
        Api {}
    }

    /// 获取插件宿主运行路径
    pub fn get_host_path(&self) -> String {
        "宿主运行路径".to_string()
    }
}

#[derive(Debug)]
pub struct Info {
    /// 插件名称
    pub name: String,
    /// 插件 id
    pub id: String,
    /// 版本信息
    pub version: String,
    /// 作者
    pub author: String,
    /// 编译日期
    pub compile_date: String,
    /// 描述信息
    pub description: String,
    /// 是否需要前端展示结果
    pub display_result: bool,
}

impl Info {
    pub fn new(
        name: String,
        id: String,
        version: String,
        author: String,
        compile_date: String,
        description: String,
        display_result: bool,
    ) -> Info {
        Info {
            name,
            id,
            version,
            author,
            compile_date,
            description,
            display_result,
        }
    }
}
