import { RouteRecordRaw, createRouter, createWebHashHistory } from 'vue-router'
import Home from './page-home.vue'
import HomeMain from './home-main.vue'
import NotFound from './not-found.vue'; // 假设有一个404页面组件

const routes: Array<RouteRecordRaw> = [
  {
    path: '/',
    redirect: '/home', // 重定向到/home
  },
  {
    path: '/home',
    component: Home,
    children: [
      {
        path: '/main',
        component: HomeMain,   // 子路由  
      }, {
        path: '/about',
        component: () => import("./home-about.vue"),   // 子路由  
      },

    ],
  },
  {
    path: '/hotkey',
    name: 'Hotkey',
    component: () => import("./page-hotkey.vue"),
    children: [
      {
        path: '/main',
        component: NotFound,   // 子路由  
      }, {
        path: '/ai_translate',
        component: () => import("./hotkey-translate.vue"),   // 子路由  
      },
    ],
  },
  {
    path: '/:catchAll(.*)*', // 使用带有正则表达式的动态路由段来捕获所有路由
    name: 'NotFound',
    component: NotFound,
  },
]

const router = createRouter({
  history: createWebHashHistory(),
  routes,
})

export default router
