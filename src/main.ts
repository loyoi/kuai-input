import { createApp } from 'vue'
import App from './App.vue'
import { createPinia } from 'pinia'
import router from './router'
import './style.css'

import 'virtual:svg-icons-register'
import _alert from './plugin/alert/index.js'
import _msg from './plugin/message/index.js'
import _confirm from './plugin/confirm/index.js'

window.loyoi = {}
window._alert = _alert
window._msg = _msg
window._confirm = _confirm

// https://vip.123pan.cn/1815078744/DCDB/butler/data.json
;(async () => {
  // 版本
  window._ver = vite_ver
  window.config = vite_config
  //  window._ver = '1.0.0'
  // window._host = `https://vip.123pan.cn/1815078744/DCDB/butler/`

  /**
    if (import.meta.env.MODE === 'development') {
    console.log('Development environment')
  } else if (import.meta.env.MODE === 'production') {
    console.log('Production environment')
  } else {
    console.log('Other environment')
  }
   */


 

  // function openInNewTab(url:string) {
  //   const a = document.createElement('a')
  //   a.href = url
  //   a.target = '_blank'
  //   a.style.display = 'none'
  //   document.body.appendChild(a)
  //   a.click()
  //   document.body.removeChild(a)
  // }

  const app = createApp(App)
  app.use(createPinia()) // 状态管理
  app.use(router) // 路由
  app.mount('#app')


})()
