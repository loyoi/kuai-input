/// <reference types="vite/client" />

declare module '*.vue' {
  import { DefineComponent } from 'vue'
  // eslint-disable-next-line @typescript-eslint/no-explicit-any, @typescript-eslint/ban-types
  const component: DefineComponent<{}, {}, any>
  export default component
}

declare const vite_ver: string
declare const vite_config: _Config

declare interface msgOpt {
  text: string
  type?: 'success' | 'warn' | 'error'
  duration?: number
}



interface Window {
  process: any

  /**
   * 二次确认
   * @param msg 提示信息
   * @param callback 回调信息
   */
  _confirm: (msg: string, callback: (is: boolean) => void, title: any) => void

  /**
   * 打印函数
   */
  _alert: (msg: string, callback?: () => void) => void
  /**
   * 提示信息
   */
  _msg(options: msgOpt): void

  _host: string
  _ver: string

  config: _Config
}

type _Config = {
  ver: string
  product: ListItem[]
}

type ListItem = {
  /**项目名字 */
  name: string
  /**是否安装 */
  install?: boolean
  id: string
  /**连接 */
  url?: string
  /**描述 */
  des: string
  /**主图 */
  img?: string
  /**英文名 */
  en: string
  /**拓展已经安装的路径 */
  path?: string
}

interface left_list_type {
  name: string;
  id: string;
  activate: boolean;
}[]