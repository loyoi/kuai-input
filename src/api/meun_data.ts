import { ContextMenuOptions } from '@/components/ContextMenu/types'
export const items_data: ContextMenuOptions[] = [
  {
    name: '系统',
    icon: '系统',
    children: [
      {
        name: '任务管理器',
        icon: '任务管理器',
        type: 'start',
        data: 'taskmgr.exe',
      },
      {
        name: '环境变量',
        icon: '环境变量',
        type: 'self',
        data: 'hjbl',
      },
    ],
  },
  {
    name: 'Ai应用',
    icon: 'AI',
    children: [
      {
        name: '翻译为中文',
        icon: '翻译',
        type: 'plugin',
        data: 'ai_translate',
      }, {
        name: '我的测试',
        icon: '翻译',
        type: 'plugin',
        data: 'my_plugin',
      },
    ],
  },
  {
    name: '文件夹',
    children: [{
      name: '解散文件夹',
      type: 'aiapp',
      data: 'ai_translate',
    }, {
      name: '删除文件夹',
      type: 'aiapp',
      data: 'ai_translate',
    }],
  },
]