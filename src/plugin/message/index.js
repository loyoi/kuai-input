

import { createVNode, render } from 'vue'
import myMessage from './message.vue'
// 动态创建一个DOM容器
//warn 警告  error 错误  success 成功
//{ text: '邮箱有误', type: 'error', duration: 50000 }
export default ({ text, type, duration = 1200 }) => {
  text = text.toString()
  let msgDiv = document.getElementById('message')
  const appDiv = document.getElementById('app')

  if (msgDiv === null) {
    msgDiv = document.createElement('div')
    msgDiv.id = 'message'
    msgDiv.style.position = 'absolute'
    msgDiv.style.zIndex = '9999'
    msgDiv.style.width = '100%'
    appDiv.parentNode.insertBefore(msgDiv, appDiv)
  }

  const div = document.createElement('div')
  div.style.width = '100%'
  div.style.display = 'flex'
  div.style.flexDirection = 'column'
  div.style.alignItems = 'stretch'
  // div.setAttribute('class', 'my-message-container')
  const vnode = createVNode(myMessage, { text, type, duration })
  render(vnode, div)

  msgDiv.appendChild(div)

  if (window._timeoutId) {
    clearTimeout(window._timeoutId) // 清除之前设置的延迟
  }

  window._timeoutId = setTimeout(() => {
    if (duration === 0) return
    document.body.removeChild(msgDiv)
  }, duration + 1200)
}
