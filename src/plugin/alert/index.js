/* eslint-disable no-undef */
import myAlert from "./alert.vue";
import { createVNode, render } from "vue";

// export declare const _alert: (msg: string, callback?: (() => void)) => void;
/**
 *  @param {string} msg 提示的信息
 *  @param {(is:boolean)=>void} callback 返回确定结果
 */
export default (msg) => {
  const div = document.createElement("div");
  div.style.position = "absolute";
  div.style.zIndex = 999;
  div.style.top = "0";
  div.style.width = "100%";
  div.style.height = "100%";
  // data.div = div;
  const vnode = createVNode(myAlert, { msg, div });
  render(vnode, div);

  document.body.appendChild(div);

  div.addEventListener("click", () => {
    if (typeof callback == "function") {
      callback(false);
    }
    document.body.removeChild(div);
  });
};
