import { createVNode, render } from "vue";
import myMore from "./more.vue";
// import { mask } from "./index.d.ts";

function more(data) {
  const div = document.createElement("div");
  div.id = "my-more";
  div.style.position = "absolute";
  div.style.zIndex = 999;
  div.style.top = "0";
  div.style.width = "100%";
  div.style.height = "100%";

  const vnode = createVNode(myMore, {
    imgs: data.imgs,
    md: data.md,
    title: data.name,
  });
  render(vnode, div);

  document.body.appendChild(div);
}

function mask(node, data, opt) {
  const e = opt.event;
  const shift = opt.shift;

  const div = document.createElement("div");
  div.classList.add("mask");
  const vnode = createVNode(node, data);

  render(vnode, div);
  const a = document.body.appendChild(div);
  const b = div.children[0];

  /**如果是函数，就执行 可以是字符串，可以什么都没有*/
  // eslint-disable-next-line valid-typeof
  if (typeof shift === "custom") {
    opt.func(b, opt.event);
  } else if (shift === "center") {
    // 弹出窗全屏居中
    b.classList.add("ad-center");
  } else if (shift === "vertical") {
    // 垂直显示
    const screenHeight = window.innerHeight;
    const screenWidth = window.innerWidth;

    const width = b.offsetWidth;
    const height = b.offsetHeight;

    let top = e.clientY + 15;

    // 如果提示框超出屏幕底部，则显示在上方
    if (e.clientY + height + 20 > screenHeight) {
      top = top - (height + 22);
    }

    let left = e.clientX - width / 2;

    // 如果提示框左边界超出屏幕，则将其置于屏幕左边界
    if (left <= 0) {
      left = 5;
    }

    // 如果提示框右边界超出屏幕，则将其置于屏幕右边界
    if (left + width > screenWidth) {
      left = screenWidth - width - 5;
    }

    b.style.left = `${left}px`;
    b.style.top = `${top}px`;
  } else {
    let X, Y;
    const mainW = document.documentElement.clientWidth;
    const mainH = document.documentElement.clientHeight;
    // 保护菜单不超过屏幕
    if (b.offsetWidth + e.clientX > mainW) {
      var x = b.offsetWidth + e.clientX - mainW + 5;
      X = e.clientX - x + "px";
    } else {
      X = e.clientX + "px";
    }

    if (b.offsetHeight + e.clientY > mainH) {
      var y = b.offsetHeight + e.clientY - mainH + 5;
      Y = e.clientY - y + "px";
    } else {
      Y = e.clientY + "px";
    }

    b.style.left = X;
    b.style.top = Y;
  }

  a.addEventListener("click", () => {
    document.body.removeChild(a);
    if (typeof opt.callback == "function") {
      opt.callback("false");
    }
  });
}

export { more, mask };
