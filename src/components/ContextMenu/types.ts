export interface ContextMenuInstance {
  id: number
  destroy: () => void
}

export interface ContextMenuProps {
  items: ContextMenuOptions[]
  on: (item: MenuItem) => void
}

export interface ContextMenuOptions {
  name: string
  children: MenuItem[]
  icon?: string
}

export interface MenuItem {
  name: string
  icon?: string
  data:string
  type:string
  // onClick?: () => void
}