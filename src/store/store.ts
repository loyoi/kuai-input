import { defineStore } from 'pinia'
import { ref, type Ref } from 'vue'

export const useMyStore = defineStore('my_store_fy', () => {
  const fy = ref('翻译中。。。')

  /**记录鼠标位置 */
  const mouse_position = ref({ x: 0, y: 0 })

  const left_list: Ref<left_list_type[]> = ref([
    {
      name: '主页',
      id: "main",
      activate: true,
    },
    {
      name: '关于',
      id: "about",
      activate: false,
    },
  ])


  return { left_list, fy, mouse_position }
})
