import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import { version } from './package.json'
import path from 'path'
import config from './config.json'
import { createSvgIconsPlugin } from "vite-plugin-svg-icons";

// https://vitejs.dev/config/
export default defineConfig(async () => ({
  plugins: [
    vue(),
    createSvgIconsPlugin({
      // 指定 SVG图标 保存的文件夹路径
      iconDirs: [path.resolve(process.cwd(), "src/assets/icons")],
      // 指定 使用svg图标的格式
      symbolId: "svg-[dir]-[name]",
    }),
  ],

  // Vite options tailored for Tauri development and only applied in `tauri dev` or `tauri build`
  //
  // 1. prevent vite from obscuring rust errors
  clearScreen: false,
  // 2. tauri expects a fixed port, fail if that port is not available
  server: {
    port: 1420,
    strictPort: true,
    watch: {
      // 3. tell vite to ignore watching `src-tauri`
      ignored: ['**/src-tauri/**'],
    },
  }, 
  resolve: {
    alias: {
      '@': path.resolve('src'),
    }
  },
  define: {
    vite_ver: JSON.stringify(version),
    vite_config: JSON.stringify(config),
  },
}))
