use rfd::{MessageButtons, MessageDialog, MessageLevel};
pub fn panic_dialog(msg: &str) {
    let msg = format!("{}\n\n{}", msg, "请将此问题汇报到 Github 问题追踪器");
    MessageDialog::new()
        .set_level(MessageLevel::Error)
        .set_title("Clash Nyanpasu Crash")
        .set_description(msg.as_str())
        .set_buttons(MessageButtons::Ok)
        .show();
}

// pub fn migrate_dialog(msg: &str) -> bool {
//     MessageDialog::new()
//         .set_level(MessageLevel::Warning)
//         .set_title("Clash Nyanpasu Migration")
//         .set_buttons(MessageButtons::YesNo)
//         .set_description(msg)
//         .show()
// }

// pub fn error_dialog(msg: String) {
//     MessageDialog::new()
//         .set_level(MessageLevel::Error)
//         .set_title("Clash Nyanpasu Error")
//         .set_description(msg.as_str())
//         .set_buttons(MessageButtons::Ok)
//         .show();
// }
