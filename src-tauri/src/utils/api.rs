use crate::statu;
// use crate::utils::dirs
use crate::trace_err;
use elevated_command::Command;
use enigo::{Enigo, Mouse, Settings};

// use std::collections::HashMap;
use std::process::{Command as StdCommand, ExitStatus, Output};

// use super::dirs;
use tauri::window::WindowBuilder;
use tauri::{AppHandle, Manager, PhysicalPosition, PhysicalSize, WindowUrl};

use super::dirs;

/// 创建主窗口，如果窗口存在，会激活已经存在的窗口
pub fn create_main_window(app: &AppHandle, label: &str) {
    let app_url = WindowUrl::App(label.parse().unwrap());

    let _window: tauri::Window = WindowBuilder::new(app, label, app_url)
        .title("LoYoi启动器")
        .inner_size(680.0, 400.0) // 设置窗口大小
        .visible(false)
        .skip_taskbar(false) // 不在任务栏显示图标
        .center() // 窗口居中显示
        .build()
        .unwrap();
}

/// 创建热键创建窗口，如果窗口存在，会激活已经存在的窗口
pub fn create_hotkey_window(app: &AppHandle, label: &str) {
    let app_url = WindowUrl::App(label.parse().unwrap());

    let window: tauri::Window = WindowBuilder::new(app, label, app_url)
        .decorations(true) // 设置窗口无边框
        .fullscreen(true) // 设置全屏启动
        // .content_protected(true) // 设置窗口内容受保护
        .title("loyoi")
        .transparent(true) // 设置窗口透明
        .skip_taskbar(true) // 不在任务栏显示图标
        .build()
        .unwrap();

    // 获取窗口大小
    let size = window.inner_size().unwrap();
    let point = get_new_position(size);
    // 设置窗口位置
    window.set_position(point).unwrap();

    // let hwnd_as_ptr: *mut std::ffi::c_void = hwnd.0 as *mut _;

    // let hwnd = window.hwnd().unwrap(); // 获取窗口句柄
    // let api_hwnd = hwnd.0 as winapi::shared::windef::HWND;
    // println!("hwnd: {:p}", hwnd_as_ptr);

    // 将 windows::core::HWND 转换为原始指针
    // let hwnd_ptr = hwnd.as_raw() as *mut std::ffi::c_void;
    // crate::win::set_window_mouse_transparency(hwnd); // 设置窗口鼠标透明

    set_focus(window);
    // let _ = window.set_focus();
}

/// 激活或创建指定名称的窗口
pub fn activate_window(app: &AppHandle, name: &str) {
    log::info!("activate window: {}", name);
    if let Some(win) = app.get_window(name) {
        set_focus(win); // 存在：激活窗口
    } else {
        match name {
            dirs::MAIN_WIN => create_main_window(app, name),
            dirs::HOTKEY_WIN => create_hotkey_window(app, name),
            _ => {} // 可以添加错误处理或日志记录
        }
    };
}

/// 设置窗口激活，更新位置
pub fn set_focus(win: tauri::window::Window) {
    if win.is_minimized().unwrap() {
        trace_err!(win.unminimize(), "set win unminimize"); // 取消最小化
    } else {
        trace_err!(win.show(), "显示窗口");
    }

    // let size = win.inner_size().unwrap();
    // win.set_position(get_new_position(size)).unwrap();

    trace_err!(win.set_focus(), "设置激活");
    trace_err!(win.set_always_on_top(true), "窗口置顶");
    // 等待一小段时间，然后取消置顶，以确保窗口在最前面显示
    std::thread::sleep(std::time::Duration::from_millis(220));
    trace_err!(win.set_always_on_top(false), "取消窗口置顶");
}

/// 获取鼠标位置
pub fn get_mouse_position() -> statu::Point {
    let enigo = Enigo::new(&Settings::default()).unwrap();
    // 获取鼠标位置
    let t = enigo.location().unwrap();

    statu::Point::new(t.0, t.1)
}

pub fn my_menu() {
    println!("my_menu");
//     use muda::{
//         accelerator::{Accelerator, Code, Modifiers},
//         ContextMenu, Menu, MenuEvent, MenuItem, PredefinedMenuItem, Submenu,
//     };
//     let menu = Menu::new();
//     let menu_item2 = MenuItem::new("Menu item #2", false, None);
//     let _ = menu.append_items(&[&menu_item2]);
//     let position = dpi::PhysicalPosition { x: 100., y: 120. };
//     menu.show_context_menu_for_hwnd(hwnd.0, Some(position.into()));
}

/// 计算新的位置数据
pub(crate) fn get_new_position(size: PhysicalSize<u32>) -> PhysicalPosition<u32> {
    let point = get_mouse_position();

    // 计算新的位置，对窗口做边缘保护
    let new_x = if point.x > size.width as i32 {
        (point.x - (size.width / 2) as i32) as u32
    } else {
        0 // 或者采取其他处理方式
    };

    let new_y = if point.y > size.height as i32 {
        (point.y - (size.height / 2) as i32) as u32
    } else {
        0 // 或者采取其他处理方式
    };

    // log::info!("point: {:?}, new_x: {}, new_y: {}", point, new_x, new_y);
    // log::info!("x: {}, y: {}", point.0, point.1);

    PhysicalPosition::new(new_x, new_y)
}

/// 执行管理员权限命令
pub fn admin(name: &str, args: &[&str]) -> Output {
    let is_elevated = Command::is_elevated();
    let mut cmd = StdCommand::new(name);
    cmd.args(args);

    let output = if is_elevated {
        cmd.output().unwrap()
    } else {
        #[cfg(target_os = "windows")]
        {
            // println!("Running as admin: {:?}",cmd);
            use std::os::windows::process::ExitStatusExt;
            let elevated_cmd = Command::new(cmd);
            match elevated_cmd.output() {
                Ok(opt) => opt,
                Err(_) => Output {
                    status: ExitStatus::from_raw(5),
                    stdout: Vec::<u8>::new(),
                    stderr: Vec::<u8>::new(),
                },
            }
        }

        #[cfg(target_os = "macos")]
        {
            use std::os::unix::process::ExitStatusExt;
            let mut elevated_cmd: Command = Command::new(cmd);
            elevated_cmd.name("LoYoi后期管家".to_string());
            match elevated_cmd.output() {
                Ok(opt) => opt,
                Err(_) => Output {
                    status: ExitStatus::from_raw(5),
                    stdout: Vec::<u8>::new(),
                    stderr: Vec::<u8>::new(),
                },
            }
        }
    };

    output
}
