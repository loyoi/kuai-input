use crate::utils::{api, dialog, dirs};
use tauri::{App, AppHandle, GlobalShortcutManager, RunEvent};

// #[derive(Clone, serde::Serialize)]
// struct Payload {
//     cwd: String,
// }

/// 设置快捷键事件
fn set_shortcut_event(app: &AppHandle, shortcut: &str) {
    // 取消所有快捷键的注册
    app.global_shortcut_manager().unregister_all().unwrap();
    // 检查快捷键是否注册
    let is_registered = app
        .global_shortcut_manager()
        .is_registered(shortcut)
        .unwrap();

    // 如果快捷键未注册
    if is_registered {
        dialog::panic_dialog("快捷键已注册");
    } else {
        register_hotkey(app, shortcut);
    }
}

/// 注册快捷键
fn register_hotkey(app: &AppHandle, shortcut: &str) {
    // 注册快捷键
    let window = app.clone();
    // 是否注册成功
    if let Err(e) = app
        .global_shortcut_manager()
        .register(shortcut, move || super::hotkey::hotkey_event(&window))
    {
        log::info!("注册失败: {}", e);
        dialog::panic_dialog(&e.to_string());
    }
}


///  加载插件数据
fn load_plugins() -> Result<String, anyhow::Error> {
   
    Ok("".to_string())
}

/// 初始化app
pub fn app(app: &mut App) -> Result<(), Box<(dyn std::error::Error)>> {
    let app_handle = app.handle();

    let pg_info = load_plugins()?;

    println!("pg_info: {}", pg_info);
    // utils::handle::Handle::global().init(app_handle.clone());

    // let mut store_guard = crate::utils::store::MY_STORE.lock().unwrap();
    // store_guard.set_my_hwnd(s);

    // let clipboard = app.state::<tauri_plugin_clipboard::ClipboardManager>();
    // let files = clipboard
    //     .read_text()
    //     .unwrap();
    // log::info!("files: {:?}", files);

    set_shortcut_event(&app_handle, "Alt+F1");

    Ok(())
}

/// 初始化单例插件
pub fn single_instance(app: &AppHandle, _argv: Vec<String>, _cwd: String) {
    api::activate_window(app, dirs::MAIN_WIN);
}

pub fn run(app: &AppHandle, event: RunEvent) {
    match event {
        RunEvent::ExitRequested { api, .. } => {
            api.prevent_exit();
        }

        RunEvent::Ready => {
            log::info!("启动成功，运行中。。。");
            api::activate_window(app, dirs::MAIN_WIN);
        }

        RunEvent::Exit => {
            // 退出程序
            // resolve::resolve_reset();
            tauri::api::process::kill_children();
            app.exit(0);
        }

        #[cfg(target_os = "macos")]
        RunEvent::WindowEvent { label, event, .. } => {
            use tauri::Manager;

            if label == "main" {
                if let tauri::WindowEvent::CloseRequested { api, .. } = event {
                    api.prevent_close();
                    let _ = resolve::save_window_state(app_handle, true);

                    if let Some(win) = app_handle.get_window("main") {
                        let _ = win.hide();
                    }
                }
            }
        }

        #[cfg(not(target_os = "macos"))]
        RunEvent::WindowEvent { label, event, .. } => {
            if label == dirs::MAIN_WIN.to_string() {
                match event {
                    // 窗口缩放
                    tauri::WindowEvent::ScaleFactorChanged { scale_factor, .. } => {
                        println!("scale_factor: {}", scale_factor);
                        // core::tray::on_scale_factor_changed(scale_factor);
                    }

                    // 窗口关闭窗口销毁
                    tauri::WindowEvent::CloseRequested { .. } | tauri::WindowEvent::Destroyed => {
                        // log::info!(target: "app", "window close requested");
                        // let _ = resolve::save_window_state(app_handle, true);
                    }

                    // 窗口移动，改变大小
                    tauri::WindowEvent::Moved(_) | tauri::WindowEvent::Resized(_) => {
                        // log::info!(target: "app", "window moved or resized");
                        // std::thread::sleep(std::time::Duration::from_nanos(1));
                        // let _ = resolve::save_window_state(app_handle, false);
                    }

                    // 窗口丢失焦点
                    tauri::WindowEvent::Focused(false) => {
                        println!("焦点丢失");
                        // app.get_window(dirs::APP_NAME).unwrap().close().unwrap();
                    }

                    _ => {}
                }
            }
        }

        _ => {}
    }
}
