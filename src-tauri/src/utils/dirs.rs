

#[cfg(not(feature = "verge-dev"))]

/// 主窗口
pub const MAIN_WIN: &str = "loyoi-launcher-main";

/// 热键窗口
pub const HOTKEY_WIN: &str = "loyoi-launcher-hotkey";

#[cfg(feature = "verge-dev")]
pub const APP_NAME: &str = "kuai-input-dev";
