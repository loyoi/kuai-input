use libloading::{Library, Symbol};
use plugin_interface::Plugin;
use std::error::Error;

#[derive(Debug)]
pub enum LoadingError {
    LibraryLoad(libloading::Error),
    SymbolLoad(libloading::Error),
}

impl std::fmt::Display for LoadingError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            LoadingError::LibraryLoad(e) => write!(f, "Failed to load library: {}", e),
            LoadingError::SymbolLoad(e) => write!(f, "Failed to load symbol: {}", e),
        }
    }
}

impl Error for LoadingError {}

pub struct LoadLib {}

impl LoadLib {
    pub fn new() -> Self {
        Self {}
    }

    pub fn load_file(&self, path: String) -> Result<Library, LoadingError> {
        unsafe { Library::new(path).map_err(LoadingError::LibraryLoad) }
    }

    pub fn get_service(&self, lib: &Library) -> Result<Box<dyn Plugin>, LoadingError> {
        let new_service: Symbol<extern "Rust" fn(String) -> Box<dyn Plugin>> =
            unsafe { lib.get(b"new_plugin").map_err(LoadingError::SymbolLoad) }?;
        let service = new_service("my_service".to_string());
        Ok(service)
    }
}
