use crate::store::MY_STORE;
use crate::utils::{api, dirs};
use tauri::AppHandle;

/// 快捷键事件处理函数
pub fn hotkey_event(win: &AppHandle) {
    // 获取全局状态
    let store_guard = MY_STORE.get().unwrap();

    // 锁定全局状态
    let mut store = store_guard.lock();

    // 获取当前激活窗口句柄
    let s = crate::win::get_activate_window();

    // 保存当前激活窗口句柄
    store.set_my_hwnd(s);

    // 保存当前 AppHandle
    store.set_app_handle(win.clone()); 
    
    log::info!("快捷键事件: {:?}", s);

    api::my_menu();

    // // 激活窗口 dirs::MAIN_APP_NAME
    // api::activate_window(&win, dirs::HOTKEY_WIN);
}
