use enigo::{
    Direction::{Click, Press, Release},
    Enigo, Key, Keyboard, Settings,
};

use windows::Win32::{
    Foundation::HWND,
    UI::WindowsAndMessaging::{GetForegroundWindow, SetForegroundWindow},
};

// 获取当前打开的文件浏览器窗口所指向的文件夹路径并返回
pub fn win_ctrl_c() -> Result<(), anyhow::Error> {
    let mut enigo = Enigo::new(&Settings::default()).unwrap();

    up_control_keys(&mut enigo);

    enigo.key(Key::Control, Press).unwrap();
    #[cfg(target_os = "windows")]
    enigo.key(Key::C, Click).unwrap();

    #[cfg(target_os = "linux")]
    enigo.key(Key::Unicode('c'), Click).unwrap();
    enigo.key(Key::Control, Release).unwrap();

    Ok(())
}

pub fn get_activate_window() -> HWND {
    unsafe {
        // 获取当前激活窗口的句柄
        let hwnd = GetForegroundWindow();
        hwnd
    }
}

/// 传入句柄，设置窗口鼠标穿透
// pub fn set_window_mouse_transparency(hwnd: HWND) {
//     println!("set_window_mouse_transparency {:?}", hwnd);
    // let current_style = unsafe { SetWindowLongPtrW(hwnd, GWL_EXSTYLE) };
// }

/// 获取窗口的类名,传入句柄
// pub fn get_class_name(hwnd: HWND) -> Result<String, anyhow::Error> {
//     // 创建变量以存储文件夹路径信息
//     use winapi::um::winuser::{GetWindowTextLengthW,GetWindowTextW};
//     unsafe {
//         // let mut class_name = [0; 260]; // 创建数组以存储窗口类名
//         let length = GetWindowTextLengthW(hwnd) as usize;
//         // 为窗口标题分配一个足够大的缓冲区
//         let mut buffer: Vec<u16> = vec![0; length + 1];
//         // 获取当前窗口的类名
//         GetWindowTextW(hwnd, buffer.as_mut_ptr(), (length + 1) as i32);
//         // 查找类名数组中的空字符位置并创建类名字符串
//         let pos = buffer.iter().position(|c| *c == 0).unwrap();
//         Ok(String::from_utf16_lossy(&buffer[0..pos]))
//     }
// }

// 检查窗口是否激活
pub fn is_window_active(hwnd: HWND) -> bool {
    unsafe { GetForegroundWindow() == hwnd }
}

// 手动激活窗口
pub fn activate_window(hwnd: HWND) {
    unsafe { let _ =SetForegroundWindow(hwnd); };
}

#[allow(dead_code)]
#[cfg(not(target_os = "macos"))]
/// 释放所有控制键
pub fn up_control_keys(enigo: &mut Enigo) {
    enigo.key(Key::Control, Release).unwrap();
    enigo.key(Key::Alt, Release).unwrap();
    enigo.key(Key::Shift, Release).unwrap();
    enigo.key(Key::Space, Release).unwrap();
    enigo.key(Key::Tab, Release).unwrap();
}

#[allow(dead_code)]
#[cfg(target_os = "macos")]
/// 释放所有控制键
pub fn up_control_keys(enigo: &mut Enigo) {
    enigo.key(Key::Control, Release).unwrap();
    enigo.key(Key::Meta, Release).unwrap();
    enigo.key(Key::Alt, Release).unwrap();
    enigo.key(Key::Shift, Release).unwrap();
    enigo.key(Key::Space, Release).unwrap();
    enigo.key(Key::Tab, Release).unwrap();
    enigo.key(Key::Option, Release).unwrap();
}

mod tests {
    #[test]
    fn my_test() {
        let s = super::get_activate_window();
        println!("{:?}", s);
    }
}
