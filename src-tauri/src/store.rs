use once_cell::sync::OnceCell;
use parking_lot::Mutex; // 使用 parking_lot 中的 Mutex 以提供更好的性能
use std::sync::Arc;
use tauri::AppHandle;
// use winapi::shared::windef::HWND;
use windows::Win32::Foundation::HWND;

// 状态管理

#[derive(Debug, Clone)]
pub struct Store {
    my_hwnd: String, // 存储为字符串
    app_handle: Arc<Mutex<Option<AppHandle>>>,
}

impl Store {
    fn new() -> Self {
        Store {
            my_hwnd: "".to_string(),
            app_handle: Arc::new(Mutex::new(None)),
        }
    }

    pub fn set_my_hwnd(&mut self, hwnd: HWND) {
        // 将 HWND 转换为十六进制字符串并存储
        self.my_hwnd = format!("{:#X}", hwnd.0 as usize);
    }

    pub fn get_app_handle(&self) -> Option<AppHandle> {
        self.app_handle.lock().clone()
    }

    pub fn set_app_handle(&self, app_handle: AppHandle) {
        *self.app_handle.lock() = Some(app_handle);
    }

    pub fn get_my_hwnd(&self) -> Result<HWND, anyhow::Error> {
        if self.my_hwnd == "".to_string() {
            return Err(anyhow::anyhow!("Failed to parse HWND"));
        }

        let hwnd_str = self.my_hwnd.as_str();
        let trimmed_hwnd_str = hwnd_str.trim_start_matches("0x");
        let hwnd_val = u64::from_str_radix(trimmed_hwnd_str, 16)
            .map_err(|_| anyhow::anyhow!("Failed to parse HWND value from string: {}", hwnd_str))?;
        // 在windows crate中，HWND是一个结构体，其内部包含了原始句柄值，因此可以直接从u64构造。
        Ok(HWND(hwnd_val.try_into().unwrap()))
    }
}

// 使用 OnceCell 和 Mutex 实现全局存储
pub static MY_STORE: OnceCell<Mutex<Store>> = OnceCell::new();

pub fn init_store() {
    let store = Store::new();
    MY_STORE
        .set(Mutex::new(store))
        .expect("Store should be set only once");
}
