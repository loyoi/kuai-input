use std::{os::windows::process::CommandExt, time::Duration};

use crate::utils::{api, load_lib};
use anyhow::anyhow;
use enigo::{Coordinate, Mouse, Settings};

use std::process::Command;

use crate::store::MY_STORE;
use plugin_interface::PluginCtx;
use tauri_plugin_clipboard::ClipboardManager;

#[tauri::command]
pub fn greet(name: &str) -> String {
    // let app = tauri::api::process::CommandEvent;
    println!("Hello, {}! You've been greeted from Rust!", name);
    format!("Hello, {}! You've been greeted from Rust!", name)
}

#[tauri::command]
pub fn current_move_pos() {
    let mut ei = enigo::Enigo::new(&Settings::default()).unwrap();
    let _ = ei.move_mouse(3, 3, Coordinate::Rel);
    // let _ = ei.move_mouse(-5, 0, Coordinate::Rel);
    log::info!("鼠标位置移动了1个像素")
}

#[tauri::command]
pub fn open_task_manager() {
    // 使用 `taskmgr` 命令来打开任务管理器
    let output = api::admin("cmd", &["/C", "start", "taskmgr.exe"]);

    log::info!("{:?}", output);
}

#[tauri::command]
pub fn get_mouse_position() -> String {
    let loc: crate::statu::Point = api::get_mouse_position();
    let map = serde_json::json!({
        "x":loc.x,"y":loc.y
    });
    serde_json::to_string(&map).expect("Failed to serialize JSON object")
}

#[tauri::command]
pub fn switch_win() -> String {
    let store_guard = MY_STORE.get().expect("failed to get store");
    let store = store_guard.lock();

    use tauri::Manager;

    let app = match store.get_app_handle() {
        Some(app) => app,
        None => {
            return "app handle is None".to_string();
        }
    };

    let win = match app.get_window(crate::utils::dirs::MAIN_WIN) {
        Some(win) => win,
        None => {
            return "win is None".to_string();
        }
    };

    crate::trace_err!(win.minimize(), "设置最小化");
    crate::trace_err!(win.set_fullscreen(false), "取消窗口全屏");

    let win_size = tauri::PhysicalSize::new(500, 300);
    crate::trace_err!(win.set_size(win_size), "设置大小");

    let point = api::get_new_position(win_size);
    crate::trace_err!(win.set_position(point), "设置位置");

    // std::thread::sleep(std::time::Duration::from_millis(250));
    // crate::trace_err!(win.show(), "设置显示");
    crate::trace_err!(win.unmaximize(), "取消最小化");
    crate::trace_err!(win.set_focus(), "设置激活");
    crate::trace_err!(win.set_always_on_top(true), "窗口置顶");
    // 等待一小段时间，然后取消置顶，以确保窗口在最前面显示
    std::thread::sleep(std::time::Duration::from_millis(200));
    crate::trace_err!(win.set_always_on_top(false), "取消窗口置顶");

    "".to_string()
}

/// 构建返回数据
fn build_return(is: bool, data: String) -> String {
    let result = format!(r#"{{"err":{},"data":{}}}"#, is, data);
    result
}

/// ai库，调用各种ai服务
/// 文件处理：格式转换
/// 系统工具库：强制删文件，。。。
///

#[tauri::command]
pub fn eval_cmd(t: String, d: String) -> String {
    // 使用 `taskmgr` 命令来打开任务管理器
    let res: String = match t.as_str() {
        "start" => {
            cmd_exec(vec!["start", &d]);
            "".to_string()
        }

        "self" => {
            let _ = self_fn(d);
            "".to_string()
        }

        "aiapp" => match ai_fn(d) {
            Ok(data) => build_return(false, format!("{:?}", data)), // 成功时返回数据
            Err(err) => build_return(true, format!("{:?}", err)),   // 失败时返回错误信息
        },

        _ => "".to_string(),
    };

    return res;
}

#[tauri::command]
pub async fn plugin_fn(data: String) -> String {
    match my_plugin_fn(data).await {
        Ok(s) => build_return(false, format!("{:?}", s)), // 成功时返回数据
        Err(err) => build_return(true, format!("{:?}", err)), // 失败时返回错误信息
    }
}

async fn my_plugin_fn(data: String) -> Result<String, anyhow::Error> {
    use tokio::sync::oneshot;
    let (tx, rx) = oneshot::channel();
    // 构建上下文
    let ctx = PluginCtx::Ctx::default().set_sel_text(get_sel_text()?);

    // 启动一个新的线程来运行插件
    tokio::spawn(async move {
        let result: Result<String, anyhow::Error> = (|| {
            let plugins = "E:/011Code/rust/kuai-input/target/debug";
            let pg_path_str = format!("{}/{}.dll", plugins, data);
            let ll = load_lib::LoadLib::new();
            let lib = ll.load_file(pg_path_str)?;
            let service = ll.get_service(&lib)?;

            service.run(ctx).map_err(|err| anyhow!(err))
        })();

        tx.send(result).expect("Failed to send result");
    });

    rx.await.map_err(|err| anyhow!(err))?
}

/// 执行命令行命令
fn cmd_exec(args: Vec<&str>) {
    let mut binding = std::process::Command::new("cmd");
    let binding = binding.creation_flags(0x08000000).arg("/c");
    for arg in args.iter() {
        binding.arg(arg);
    }
    let _ = binding.spawn();
}

/// 一些系统工具函数
fn self_fn(data: String) -> Result<(), anyhow::Error> {
    match data.as_str() {
        // 环境变量编辑器
        "hjbl" => {
            let _ = Command::new("rundll32")
                .args(["sysdm.cpl", "EditEnvironmentVariables"])
                .spawn()
                .expect("failed to execute process");
        }
        _ => {}
    }
    Ok(())
}

/// 获取窗口中选择的内容
fn get_sel_text() -> Result<String, anyhow::Error> {
    let store_guard = MY_STORE.get().expect("failed to get store");
    let store = store_guard.lock();
    let hwnd = store.get_my_hwnd()?;

    use tauri::Manager;
    let app = store
        .get_app_handle()
        .ok_or_else(|| anyhow::anyhow!("app handle is None"))?;

    let win = app
        .get_window(crate::utils::dirs::HOTKEY_WIN)
        .ok_or_else(|| anyhow::anyhow!("win is None"))?;

    if !crate::win::is_window_active(hwnd) {
        crate::win::activate_window(hwnd); // 如果窗口没有激活，手动激活窗口
    }

    // 初始化剪切板
    let cli = ClipboardManager::default();

    // 先写入一个空数据
    let _ = cli.write_text("".to_string());
    // 等待一段时间，确保剪切板刷新
    std::thread::sleep(Duration::from_millis(80));

    // 按下 Ctrl + C 复制内容到剪切板
    let _ = crate::win::win_ctrl_c();
    // 等待一段时间，确保内容刷新到剪切板
    std::thread::sleep(Duration::from_millis(200));

    // 读取剪切板内容
    let text = cli.read_text().map_err(|err| anyhow::anyhow!(err))?;

    // 这里需要判断剪切板内容是否为空
    if text.is_empty() || text == "" {
        return Err(anyhow::anyhow!("剪贴板为空"));
    }

    // 激活主机窗口
    api::set_focus(win);
    
    Ok(text)
}

fn ai_fn(data: String) -> Result<String, anyhow::Error> {
    let req: String = match data.as_str() {
        "ai_translate" => {
            // let win_name = crate::win::get_class_name(hwnd).unwrap();
            // log::info!("win_name: {}", win_name);

            // 先获取窗口对象

            // crate::trace_err!(win.set_fullscreen(false), "取消窗口全屏");
            // crate::trace_err!(win.hide(), "设置隐藏");

            // let win_size = tauri::PhysicalSize::new(500, 500);
            // crate::trace_err!(win.set_size(win_size), "设置大小");

            // let point = api::get_new_position(win_size);
            // crate::trace_err!(win.set_position(point), "设置位置");
            // std::thread::sleep(std::time::Duration::from_millis(250));
            // crate::trace_err!(win.show(), "设置显示");

            // crate::trace_err!(win.set_always_on_top(true), "窗口置顶");
            // 等待一小段时间，然后取消置顶，以确保窗口在最前面显示
            // std::thread::sleep(std::time::Duration::from_millis(100));
            // crate::trace_err!(win.set_always_on_top(false), "取消窗口置顶");
            // log::info!("win: {:?}", win);

            // 检查窗口是否激活

            // 粘贴板出来完成后，需要恢复窗口状态

            // 开始翻译
            // let result: String = crate::utils::ai::translate(text)?;

            // 这里需不需要将翻译后的数据写入
            // let _ = cli.write_text(result.clone());

            // result

            "".to_string()
        }

        _ => "".to_string(),
    };
    Ok(req)
}

#[test]
fn test_my_fn() {
    // let path = "taskmgr.exe";
    // let a = start_executable(path);
    // println!("{:?}",a);
}
