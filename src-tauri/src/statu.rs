use serde::{Deserialize, Serialize};


#[derive(Default, Debug, Clone, Deserialize, Serialize)]
pub struct WindowState {
    pub title: String,
    pub width: f64,
    pub height: f64,
}


/// 单例
#[derive(Clone, serde::Serialize)]
pub struct Payload {
  pub  args: Vec<String>,
  pub cwd: String,
}


#[derive(Clone, serde::Serialize,Debug)]
pub struct Point {
    pub x: i32,
    pub y: i32,
}
impl Point {
    pub fn new(x: i32, y: i32) -> Point {
        Point { x, y }
    }
}


// impl Index<&str> for WindowState {
//     type Output = String;
//     fn index(&self, index: &str) -> &Self::Output {
//         match index {
//             "title" => &self.title,
//             _ => panic!("Invalid field accessed"),
//         }
//     }
// }
