use tauri::{
    AppHandle,
    CustomMenuItem,
    SystemTray,
    SystemTrayEvent,
    SystemTrayMenu,
    SystemTrayMenuItem,
    // SystemTraySubmenu,
};

use crate::utils::{api, dirs};

// 托盘菜单
pub fn menu() -> SystemTray {
    let tray_menu = SystemTrayMenu::new()
    
        // .add_native_item(SystemTrayMenuItem::Separator) // 分割线
        .add_item(CustomMenuItem::new("set".to_string(), "设置")) // 隐藏应用窗口
        .add_item(CustomMenuItem::new("hide".to_string(), "隐藏")) // 隐藏应用窗口
        .add_item(CustomMenuItem::new("show".to_string(), "显示")) // 显示应用窗口
        .add_native_item(SystemTrayMenuItem::Separator) // 分割线
        .add_item(CustomMenuItem::new("quit".to_string(), "退出")); // 退出

    // 设置在右键单击系统托盘时显示菜单
    SystemTray::new().with_menu(tray_menu)
}

// 菜单事件
pub fn handler(app: &AppHandle, event: SystemTrayEvent) {

    // 匹配点击事件
    match event {
        // 左键点击
        #[cfg(target_os = "windows")]
        SystemTrayEvent::LeftClick { .. } => {
            println!("需要创建app");
            api::activate_window(app, dirs::MAIN_WIN);
        }

        // 右键点击
        SystemTrayEvent::RightClick {
            position: _,
            size: _,
            ..
        } => {
            println!("system tray received a right click");
        }

        // 双击，macOS / Linux 不支持
        SystemTrayEvent::DoubleClick {
            position: _,
            size: _,
            ..
        } => {
            println!("system tray received a double click");
        }

        // 根据菜单 id 进行事件匹配
        SystemTrayEvent::MenuItemClick { id, .. } => match id.as_str() {
            "edit_file" => {
                println!("Eidt File");
                // message(parent_window, "Eidt File", "TODO");
            }
            
            "new_file" => {
                println!("Eidt File");
                // message(parent_window, "New File", "TODO");
            }

            "quit" => {
                std::process::exit(0);
            }

            "show" => {
                // window.show().unwrap()
                println!("show");
            }

            "hide" => {
                println!("Hide");
                // window.hide().unwrap();
            }
            _ => {}
        },
        _ => {}
    }
}
