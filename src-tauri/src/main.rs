// Prevents additional console window on Windows in release, DO NOT REMOVE!!
#![cfg_attr(
    all(not(debug_assertions), target_os = "windows"),
    windows_subsystem = "windows"
)]


mod statu;
mod tcmd;
mod tray;
mod utils;
mod win;
pub mod store;

use utils::init;


/// 日志初始化
fn init_logger() {
    std::env::set_var("RUST_LOG", "info");

    let env = env_logger::Env::default().filter("RUST_LOG");

    env_logger::Builder::from_env(env)
        .format_level(true)
        .format_timestamp_micros()
        .init();
}

// Learn more about Tauri commands at https://tauri.app/v1/guides/features/command

fn main() -> std::io::Result<()> {
    init_logger();
    store::init_store(); // 初始化数据存储,全局只需要初始化一次

    
    tauri::Builder::default()
        .setup(init::app) // 初始化
        // .plugin(tauri_plugin_store::Builder::default().build())
        .plugin(tauri_plugin_clipboard::init()) // 粘贴板
        .plugin(tauri_plugin_single_instance::init(init::single_instance)) //注册单例
        .invoke_handler(tauri::generate_handler![
            tcmd::greet,
            tcmd::current_move_pos,
            tcmd::open_task_manager,
            tcmd::eval_cmd,
            tcmd::get_mouse_position,
            tcmd::switch_win,
            tcmd::plugin_fn,
        ])
        .system_tray(tray::menu()) // 设置托盘图标
        .on_system_tray_event(tray::handler) // 绑定托盘事件
        .build(tauri::generate_context!()) // 构建应用窗口
        .expect("error while running tauri application") // 处理可能的错误
        .run(init::run); // 循环监听事件，一般在主线程中
    Ok(())
}
